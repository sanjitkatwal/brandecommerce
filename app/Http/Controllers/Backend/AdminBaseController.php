<?php

namespace App\Http\Controllers\Backend;

use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminBaseController extends Controller
{
    protected function checkFolderExists(){
        if (!file::exists($this->folder_path)){
            //create folder
            File::makeDirectory($this->folder_path);
        }
    }
}
