<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Brand\BrandAddValidation;
use App\Http\Requests\Backend\Brand\BrandEditValidation;
use App\Models\User;
use File, DB;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BrandController extends AdminBaseController
{
    protected $folder_path;
    protected $folter_name = 'brand';

    public function __construct()
    {
       // parent::__construct();

        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folter_name.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        return view('backend.brand.index')
            ->with('brands', Brand::all())
            ->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandAddValidation $request)
    {
        //dd($this->folder_path);
        if ($request->hasFile('brand_logo')){
            parent::checkFolderExists();

            $image = $request->file('brand_logo');
            $new_image_name = rand(4747, 9876).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $new_image_name);
        }

        Brand::create([
            'created_by'        => 1,
            'title'             => $request->get('title'),
            'slug'              => str_slug($request->get('slug')),
            'short_desc'        => $request->get('short_desc'),
            'long_desc'         => $request->get('long_desc'),
            'brand_logo'        => isset($new_image_name)?$new_image_name:'No Image Uploaded',
            'status'            => $request->get('status'),
            'seo_title'         => $request->get('seo_title'),
            'seo_keywords'      => $request->get('seo_key'),
            'seo_description'   => $request->get('seo_description'),
        ]);

       $request->session()->flash('success', 'Data added Successfully.');

       return redirect(route('admin.brands.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        if(!$data['row'] = Brand::find($id)){
            dd('Invalid Request!');
        }

        return view('backend.brand.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandEditValidation $request, $id)
    {
        if(!$brand = Brand::find($id))
            dd('Invalid Request');

        if ($request->hasFile('brand_logo')){
            parent::checkFolderExists();

            if($brand->brand_logo){
                //remove old image
                File::delete($this->folder_path.$brand->brand_logo);
            }

            $image = $request->file('brand_logo');
            $new_image_name = rand(4747, 9876).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $new_image_name);
        }

        $brand->update([
            'created_by'        => 1,
            'title'             => $request->get('title'),
            'slug'              => str_slug($request->get('slug')),
            'short_desc'        => $request->get('short_desc'),
            'long_desc'         => $request->get('long_desc'),
            'brand_logo'        => isset($new_image_name)?$new_image_name:$brand->brand_logo,
            'status'            => $request->get('status'),
            'seo_title'         => $request->get('seo_title'),
            'seo_keywords'      => $request->get('seo_key'),
            'seo_description'   => $request->get('seo_description'),
        ]);

        $request->session()->flash('success', 'Data Updated Successfully.');

        return redirect(route('admin.brands.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(!$brand = Brand::find($id))
            dd('Invalid Request');

        //check image if exist then firs delete image than row
        if($brand->brand_logo){
            //remove old image
            if(File::delete($this->folder_path.$brand->brand_logo)){
                File::delete($this->folder_path.$brand->brand_logo);
            }
        }

        $brand->delete();
        $request->session()->flash('success', 'Data Deleted Successfully.');

        return redirect(route('admin.brands.index'));
    }

}
