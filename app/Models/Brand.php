<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'created_by', 'updated_by', 'title', 'slug', 'short_desc', 'long_desc', 'brand_logo',
        'status', 'seo_title', 'seo_keywords', 'seo_description'
    ];
}
