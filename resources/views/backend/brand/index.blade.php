@extends('backend.layouts.app')
@section('page_specific_css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('bower_components/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('breadcomes')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('admin.brands.index') }}"> Brand</a></li>
            <li class="active">List Data</li>
        </ol>
    </section>

@endsection

@section('content')

    @include('backend.common.message')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>SN.</th>
                            <th>Title</th>
                            <th>Brand Logo</th>
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Status</th>
                            <th>Operation</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($brands as $brand)
                            <tr>
                                <td>{!! $no++ !!}</td>
                                <td>{!! $brand->title !!}</td>
                                <td><img src="{{ asset('images/brand/'.$brand->brand_logo) }}" style="height: 70px; width: 55px" alt="No Image Uploaded"> </td>
                                <td>{!! $brand->created_at !!}</td>
                                <td>{!! $brand->created_by !!}</td>
                                <td>{!! $brand->status !!}</td>
                                <td>
                                    <a href="{{route('admin.brands.edit', ['id'=>$brand->id])}}" class="btn btn-info btn-sm">Edit</a>
                                    <a href="{{route('admin.brands.delete', ['id'=>$brand->id])}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete this item?')">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>SN.</th>
                            <th>Title</th>
                            <th>Brand Logo</th>
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Status</th>
                            <th>Operation</th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

@endsection

@section('page_specific_script')
    <!-- page script -->
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script src="{{ asset('bower_components/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/js/dataTables.bootstrap.min.js') }}"></script>
@endsection
