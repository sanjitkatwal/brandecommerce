<div class="box-body">
    <div class="form-group">
        <label for="exampleInputEmail1">Title</label>
        <input type="text" name="title" value="{{ isset($data['row'])?$data['row']->title:'' }}" class="form-control" id="" placeholder="Enter Title">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Slug</label>
        <input type="text" value="{{ isset($data['row'])?$data['row']->slug:'' }}" name="slug" class="form-control" id="" placeholder="Enter Slug">
    </div>
    <div class="form-group">
        <label for="">Short Description</label>
        <input type="text" name="short_desc" value="{{ isset($data['row'])?$data['row']->short_desc:'' }}" class="form-control" id="" placeholder="Enter Short Description">
    </div>
    <div class="form-group">
        <label for="">Long Description</label>
        <input type="text" name="long_desc" value="{{ isset($data['row'])?$data['row']->long_desc:'' }}" class="form-control" id="" placeholder="Enter Long Description">
    </div>
    @if (isset($data['row']))
        @if($data['row']->brand_logo)
            <div class="form-group">
                <label for="exampleInputFile">Brand Logo</label>
                <img src="{{ asset('images/brand/'.$data['row']->brand_logo) }}" alt="No Image Available" style="width: 60px; height: 70px;">
            </div>
        @else
            <div class="form-group">
                <label for="exampleInputFile">Brand Logo</label>
                <p>No Image Uploaded.</p>
            </div>

            @endif

    @endif
    <div class="form-group">
        <label for="exampleInputFile">Brand Logo</label>
        <input type="file"  name="brand_logo" id="">
    </div>
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control select2 select2-hidden-accessible" style="width: 15%;" tabindex="-1" aria-hidden="true">
            <option value="1" {{ isset($data['row'])&& $data['row']->status==1?'selected':'' }}>Active</option>
            <option value="0" {{ isset($data['row'])&& $data['row']->status==0?'selected':'' }}>In-active</option>
        </select>
    </div>
    <hr>
    <div class="form-group">
        <label for="">SEO Title</label>
        <input type="text" name="seo_title" value="{{ isset($data['row'])?$data['row']->seo_title:'' }}" class="form-control" id="" placeholder="Enter SEO Title">
    </div>
    <div class="form-group">
        <label for="">SEO Keywords</label>
        <input type="text" name="seo_key" value="{{ isset($data['row'])?$data['row']->seo_keywords:'' }}" class="form-control" id="" placeholder="Enter SEO Keywords">
    </div>
    <div class="form-group">
        <label for="">SEO Description</label>
        <input type="text" name="seo_description" value="{{ isset($data['row'])?$data['row']->seo_description:'' }}" class="form-control" id="" placeholder="Enter SEO Description">
    </div>
</div>
