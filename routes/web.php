<?php


Route::get('/', function () {
    return view('welcome');
});

/*
 * Backend Routes
 */
//Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['middleware' => 'auth'])->group(function () {

Route::get('admin/dashboard',               ['as'=> 'admin.dashboard',            'uses'=>'Backend\BackendController@index']);

Route::get('admin/brands',                  ['as'=> 'admin.brands.index',         'uses'=>'Backend\BrandController@index']);
Route::get('admin/brands/create',           ['as'=> 'admin.brands.create',        'uses'=>'Backend\BrandController@create']);
Route::post('admin/brands/store',           ['as'=> 'admin.brands.store',         'uses'=>'Backend\BrandController@store']);
Route::get('admin/brands/{id}/edit',        ['as'=> 'admin.brands.edit',          'uses'=>'Backend\BrandController@edit']);
Route::post('admin/brands/{id}/update',     ['as'=> 'admin.brands.update',        'uses'=>'Backend\BrandController@update']);
Route::get('admin/brands/{id}delete',       ['as'=> 'admin.brands.delete',        'uses'=>'Backend\BrandController@destroy']);

});


Auth::routes();

